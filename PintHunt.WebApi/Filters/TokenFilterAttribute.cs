﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using PintHunt.Helpers;
using PintHunt.Models;
using PintHunt.WebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PintHunt.WebApi.Filters
{
    public class TokenFilterAttribute : Attribute, IActionFilter
    {
        public GenericApiResult GetResult(bool IsSucceed, ResultCodes ResultCode, object Result = null, string language = null, string Message = null)
        {
            return new GenericApiResult()
            {
                Message = Message,
                IsSucceed = IsSucceed,
                Result = Result,
                ResultCode = (int)ResultCode
            };
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Request.Headers.Any(a => a.Key == "AuthorizationToken" || a.Key == "authorizationtoken"))
            {
                var AuthToken = context.HttpContext.Request.Headers.Where(a => a.Key == "AuthorizationToken" || a.Key == "authorizationtoken").Select(a => a.Value).FirstOrDefault().ToString();
                if (string.IsNullOrEmpty(AuthToken))
                {
                    context.Result = new ContentResult()
                    {
                        ContentType = "application/json",
                        Content = JsonConvert.SerializeObject(GetResult(false, ResultCodes.Error, Message: "Authorize Error"))
                    };
                }
                else
                {
                    var token = JwtHelper.GetToken(AuthToken);
                    if (token == null)
                    {
                        context.Result = new ContentResult()
                        {
                            ContentType = "application/json",
                            Content = JsonConvert.SerializeObject(GetResult(false, ResultCodes.Error, Message: "Not Valid"))
                        };
                    }
                    else
                    {
                        var baseController = (BaseController)context.Controller;
                        baseController.token = token;
                    }
                }
            }
            else
            {
                context.Result = new ContentResult()
                {
                    ContentType = "application/json",
                    Content = JsonConvert.SerializeObject(GetResult(false, ResultCodes.Error, Message: "Authorize Error"))
                };
            }
        }
    }
}
