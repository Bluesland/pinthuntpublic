﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PintHunt.Data;
using PintHunt.Helpers;
using PintHunt.Models;
using PintHunt.Models.DbModels;
using PintHunt.Models.Dto;

namespace PintHunt.WebApi.Controllers
{

    public class LoginController : BaseController
    {
        private readonly IDynamoDbContext<User> db;
        public LoginController(IDynamoDbContext<User> _db)
        {
            db = _db;

        }

        [HttpPost("Register")]
        public async Task<GenericApiResult> Register(UserDto user)
        {
            try
            {
                if (await db.IsExistAsync(user.Id) == false)
                {
                    if (!string.IsNullOrEmpty(user.Password))
                    {

                        PasswordHasher passwordHasher = new PasswordHasher();
                        user.PasswordHash = passwordHasher.HashPassword(user.Password);
                    }
                    await db.SaveAsync(user.GetDynamoModel());
                    return GetResult(true, ResultCodes.Success, user);

                }
                else
                {

                    return GetResult(false, ResultCodes.Error);

                }



            }
            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }

        [HttpPost]
        public async Task<GenericApiResult> Login(LoginRequest loginRequest)
        {
            try
            {
                if (loginRequest == null && string.IsNullOrEmpty(loginRequest.Username) && string.IsNullOrEmpty(loginRequest.Password))
                {
                    return GetResult(false, ResultCodes.Error);

                }

                //burada patlıyor..
                var user = await db.GetByIdAsync(loginRequest.Username);

                if (user != null)
                {
                    PasswordHasher passwordHasher = new PasswordHasher();

                    if (passwordHasher.VerifyHashedPassword(user.PasswordHash, loginRequest.Password))
                    {

                        string token = JwtHelper.GenerateToken(new TokenModel()
                        {
                            deviceType = loginRequest.DeviceType,
                            lang = loginRequest.Lang,
                            versionNo = loginRequest.VersionNo,
                            id = user.Id
                        });

                        return GetResult(true, ResultCodes.Success, new LoginSuccessResponse()
                        {
                            Token = token,
                            User = new Models.Dto.UserDto(user)

                        });
                    }
                    else
                    {
                        //farklı hata kodu dön
                        return GetResult(false, ResultCodes.Error);


                    }


                }
                else
                {
                    return GetResult(false, ResultCodes.Error);


                }


            }
            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }
    }
}