﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PintHunt.Data;
using PintHunt.Helpers;
using PintHunt.Models;
using PintHunt.Models.DbModels;
using PintHunt.Models.Dto;
using PintHunt.WebApi.Filters;

namespace PintHunt.WebApi.Controllers
{
   
    public class RouteController : BaseController
    {

        private readonly IDynamoDbContext<Route> db;
        public RouteController(IDynamoDbContext<Route> _db)
        {
            db = _db;

        }
        [HttpGet("GetAllRoute")]
        public async Task<GenericApiResult> GetRoutes()
        {
            try
            {
                List<Route> routes = await db.ScanAsync(new List<Amazon.DynamoDBv2.DataModel.ScanCondition>());

                return GetResult(true, ResultCodes.Success, routes);
            }

            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }

        [HttpPost]
        public async Task<GenericApiResult> RoutePub(RouteDto route)
        {
            try
            {
                
                   
                    await db.SaveAsync(route.GetDynamoModel());
                    return GetResult(true, ResultCodes.Success, route);

            }
            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }

    }
}