﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PintHunt.Data;
using PintHunt.Helpers;
using PintHunt.Models;
using PintHunt.Models.DbModels;
using PintHunt.Models.Dto;
using PintHunt.WebApi.Filters;

namespace PintHunt.WebApi.Controllers
{
   
    public class PubController : BaseController
    {

        private readonly IDynamoDbContext<Pub> db;
        public PubController(IDynamoDbContext<Pub> _db)
        {
            db = _db;

        }
        [HttpGet("GetAllPub")]
        public async Task<GenericApiResult> GetPubs()
        {
            try
            {
                List<Pub> pubs = await db.ScanAsync(new List<Amazon.DynamoDBv2.DataModel.ScanCondition>());

                return GetResult(true, ResultCodes.Success, pubs);
            }

            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }

        [HttpPost("SavePub")]
        public async Task<GenericApiResult> SavePub(PubDto pub)
        {
            try
            {
                
                   
                    await db.SaveAsync(pub.GetDynamoModel());
                    return GetResult(true, ResultCodes.Success, pub);

            }
            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }

    }
}