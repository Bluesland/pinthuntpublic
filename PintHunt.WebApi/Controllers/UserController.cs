﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PintHunt.Data;
using PintHunt.Helpers;
using PintHunt.Models;
using PintHunt.Models.DbModels;
using PintHunt.Models.Dto;
using PintHunt.WebApi.Filters;

namespace PintHunt.WebApi.Controllers
{
   [TokenFilter]
    public class UserController : BaseController
    {
        private readonly IDynamoDbContext<User> db;
        public UserController(IDynamoDbContext<User> _db)
        {
            db = _db;
          
        }
        [HttpGet("GetAllUser")]
        public async Task<GenericApiResult> GetUsers()
        {
            try
            {
                List<User> users = await db.ScanAsync(new List<Amazon.DynamoDBv2.DataModel.ScanCondition>());

                return GetResult(true, ResultCodes.Success, users);
            }
            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }
        [HttpGet]
        public async Task<GenericApiResult> GetUser()
        {
            try
            {
                User user = await db.GetByIdAsync(token.id);
               
                return GetResult(true, ResultCodes.Success,user);
            }
            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }


        [HttpPost]
        public async Task<GenericApiResult> SaveUser(UserDto user)
        {
            try
            {
                if (token.id == user.Id)
                {
                    if (!string.IsNullOrEmpty(user.Password))
                    {
                        PasswordHasher passwordHasher = new PasswordHasher();
                        user.PasswordHash = passwordHasher.HashPassword(user.Password);
                    }
                    await db.SaveAsync(user.GetDynamoModel());
                    return GetResult(true, ResultCodes.Success, user);
                }
                else
                {
                    return GetResult(false, ResultCodes.Error);

                
                }



                
            }
            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }

    }
}