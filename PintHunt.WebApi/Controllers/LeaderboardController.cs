﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PintHunt.Data;
using PintHunt.Models;
using PintHunt.Models.DbModels;

namespace PintHunt.WebApi.Controllers
{
    
    public class LeaderboardController : BaseController
    {
        private readonly IDynamoDbContext<Leaderboard> db;
        public LeaderboardController(IDynamoDbContext<Leaderboard> _db)
        {
            db = _db;

        }


        [HttpGet("GetAllLeaderboard")]
        public async Task<GenericApiResult> GetLeaderboards()
        {
            try
            {
                List<Leaderboard> leaderboards = await db.ScanAsync(new List<Amazon.DynamoDBv2.DataModel.ScanCondition>());

                return GetResult(true, ResultCodes.Success, leaderboards);
            }

            catch (Exception x)
            {
                LogException(x);
                return GetResult(false, ResultCodes.Error);
            }
        }


    


    }
}