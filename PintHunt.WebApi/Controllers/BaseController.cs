﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PintHunt.Models;

namespace PintHunt.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    public class BaseController : ControllerBase
    {
        public TokenModel token;
        public BaseController()
        {

        }
        public void LogException(Exception x)
        {
            if (x != null)
            {
                if (x.InnerException != null)
                {
                    Console.WriteLine("Exception Message: " + x.Message);
                    Console.WriteLine("Inner Exception:" + x.InnerException.Message);
                }
                else
                {
                    Console.WriteLine("Exception Message: " + x.Message);
                }
                Console.WriteLine("Stack Trace:" + x.StackTrace);
            }
        }
        public void LogException(Exception x, string extraMessage)
        {
            LogException(x);
            Console.WriteLine(extraMessage);
        }
        public string GetText(string name, string language = null)
        {
            if (language != null && language != "tr")
            {
                return name;
            }
            else
            {
                return name;
            }
        }
        public System.Int64 GetUtcTime()
        {
            return Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
        }
        public System.Double GetUtcMilliSecondTime()
        {
            return (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        }
        public GenericApiResult GetResult(bool IsSucceed, ResultCodes ResultCode, object Result = null, string language = null)
        {
            return new GenericApiResult()
            {
                Message = GetText(ResultCode.ToString(), language),
                IsSucceed = IsSucceed,
                Result = Result,
                ResultCode = (int)ResultCode
            };
        }
        public GenericApiResult GetResult(bool IsSucceed, ResultCodes ResultCode, string errorMessage, object Result = null)
        {
            return new GenericApiResult()
            {
                Message = errorMessage,
                IsSucceed = IsSucceed,
                Result = Result,
                ResultCode = (int)ResultCode
            };
        }
        
    }
}