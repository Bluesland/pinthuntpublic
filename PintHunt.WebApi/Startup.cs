using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PintHunt.Data;
using PintHunt.Models.DbModels;

namespace PintHunt.WebApi
{
    public class Startup
    {
        public const string AppS3BucketKey = "AppS3Bucket";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", "AKIAX3IWRWKRVSJFNVYF");
            Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", "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");
            Environment.SetEnvironmentVariable("AWS_REGION", "eu-west-1");
            var awsOptions = Configuration.GetAWSOptions();

            services.AddDefaultAWSOptions(awsOptions);
            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<Amazon.S3.IAmazonS3>();
            var client = awsOptions.CreateServiceClient<IAmazonDynamoDB>();
            var dynamoDbOptions = new DynamoDbOptions();
            ConfigurationBinder.Bind(Configuration.GetSection("DynamoDbTables"), dynamoDbOptions);
            // This is where the magic happens

            services.AddScoped<IDynamoDbContext<Pub>>(provider => new DynamoDbContext<Pub>(client, dynamoDbOptions.Pub));
            services.AddScoped<IDynamoDbContext<User>>(provider => new DynamoDbContext<User>(client, dynamoDbOptions.User));
            services.AddScoped<IDynamoDbContext<Route>>(provider => new DynamoDbContext<Route>(client, dynamoDbOptions.Route));
            services.AddScoped<IDynamoDbContext<Leaderboard>>(provider => new DynamoDbContext<Leaderboard>(client, dynamoDbOptions.Leaderboard));
            //tablolarư buraya ekle

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors("MyPolicy");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
