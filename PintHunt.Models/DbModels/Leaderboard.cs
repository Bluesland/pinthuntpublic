﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.DbModels
{
    public class Leaderboard:BaseModel
    {
        [DynamoDBProperty]
        public string Name { get; set; }
        [DynamoDBProperty]
        public string UserName { get; set; }
    }


    }

