﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.DbModels
{
    public class RouteTrip:BaseModel
    {
        [DynamoDBProperty]
        public bool RouteTripStatus { get; set; }
        [DynamoDBProperty]
        public DateTime StartTime { get; set; }
        [DynamoDBProperty]
        public DateTime EndTime { get; set; }
        [DynamoDBProperty]
        public int PintCount { get; set; }
        [DynamoDBProperty]
        public int PubNumber { get; set; }
        [DynamoDBProperty]
        public string BeforePhoto { get; set; }
        [DynamoDBProperty]
        public string EndPhoto { get; set; }

    }
}
