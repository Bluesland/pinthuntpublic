﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.DbModels
{
    public class Pub:BaseModel
    {
        [DynamoDBProperty]
        public string Name { get; set; }
        [DynamoDBProperty]
        public string Address { get; set; }
        [DynamoDBProperty]
        public double Latitude { get; set; }
        [DynamoDBProperty]
        public double Longitude { get; set; }
        [DynamoDBProperty]
        public string Description { get; set; }
        [DynamoDBProperty]
        public int RateCount { get; set; }
        [DynamoDBProperty]
        public double Rate { get; set; }
        [DynamoDBProperty]
        public string Image { get; set; }
    }
}
