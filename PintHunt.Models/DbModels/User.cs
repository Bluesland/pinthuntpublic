﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.DbModels
{
    public class User:BaseModel
    {
        [DynamoDBProperty]
        public string Name { get; set; }
        [DynamoDBProperty]
        public string UserName { get; set; }
        [DynamoDBProperty]
        public string PasswordHash { get; set; }
        [DynamoDBProperty]
        public string ProfilePicture { get; set; }
        [DynamoDBProperty]
        public List<Score> Scores { get; set; }


    }
}
