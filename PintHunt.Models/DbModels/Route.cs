﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.DbModels
{
    public class Route:BaseModel
    {
        [DynamoDBProperty]
        public string Name { get; set; }
        [DynamoDBProperty]
        public string Pubs { get; set; }
        [DynamoDBProperty]
        public DateTime StartTime { get; set; }
        [DynamoDBProperty]
        public DateTime EndTime { get; set; }
        [DynamoDBProperty]
        public int PintLimit { get; set; }
        [DynamoDBProperty]
        public double Latitude { get; set; }
        [DynamoDBProperty]
        public double Longitude { get; set; }
        [DynamoDBProperty]
        public int RateCount { get; set; }
        [DynamoDBProperty]
        public double Rate { get; set; }


    }
}
