﻿
using PintHunt.Models.DbModels;
using PintHunt.Models.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PintHunt.Models
{
    class ApiModels
    {
    }
    public class TokenModel
    {
        public string id { get; set; }
        public string lang { get; set; }
        public double exp { get; set; }
        public string deviceType { get; set; }
        public string versionNo { get; set; }
    }
    public class GenericApiResult
    {
        public bool IsSucceed { get; set; }
        public object Result { get; set; }
        public string Message { get; set; }
        public int ResultCode { get; set; }
    }
    public class ElasticResponse<T> where T : class
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public object _shards { get; set; }
        public ElasticHits<T> hits { get; set; }
    }
    public class ElasticHits<T> where T : class
    {
        public int total { get; set; }
        public decimal max_score { get; set; }
        public List<ElasticHitDetail<T>> hits { get; set; }
    }
    public class ElasticHitDetail<T> where T: class
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public decimal _score { get; set; }
        public T _source { get; set; }
    }
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string DeviceType { get; set; }
        public string Lang { get; set; }
        public string VersionNo { get; set; }


    }
    public class LoginSuccessResponse
    {
        public UserDto User { get; set; }
        public string Token { get; set; }
    }
    public class ForgotPasswordRequest
    {
        public string Email { get; set; }
    }
    public class VerifyEmailRequest : ForgotPasswordRequest
    {
        public string Code { get; set; }
    }
    public class CheckRegisterInfoRequest
    {
        public bool IsUsername { get; set; }
        public string Value { get; set; }
    }
    public class RefineSearchFilterRequest
    {
        public string Keywords { get; set; }
        public List<MapModel> CategoryPoints { get; set; }
        public List<string> SubCategoryIds { get; set; }
        public List<string> DestinationIds { get; set; }
        public long StartDate { get; set; }
        public long EndDate { get; set; }
        public short Budget { get; set; }
        public PaginationModel PaginationModel { get; set; }
    }
    public class ExperienceRequest
    {
        public string ExperienceId { get; set; }
        public bool isAdded { get; set; }
    }
    public class UserRequest : User
    {
        public List<string> DestinationIds { get; set; }
        public List<MapModel> CategoryIds { get; set; }
        public string Password { get; set; }
    }
    public class UserInfoRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public short Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public short PreferedLanguage { get; set; }
    }
    public class UserPasswordRequest
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
    public class FileUploadRequest
    {
        public string Stream { get; set; }
        public string FileName { get; set; }
    }
    public class MapModel
    {
        public string Key { get; set; }
        public short Value { get; set; }
    }
    public class PaginationModel
    {
        public short Page { get; set; }
        public short RowCount { get; set; }
    }
   
}
