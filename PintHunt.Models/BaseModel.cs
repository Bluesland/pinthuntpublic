﻿using Amazon.DynamoDBv2.DataModel;
using System;

namespace PintHunt.Models
{
    public class BaseModel
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        [DynamoDBProperty]
        public string CreatedBy { get; set; }
        [DynamoDBProperty]
        public long CreatedOn { get; set; }
        [DynamoDBProperty]
        public string ModifiedBy { get; set; }
        [DynamoDBProperty]
        public long ModifiedOn { get; set; }
        [DynamoDBProperty]
        public bool IsActive { get; set; }
        [DynamoDBProperty]
        public bool IsDeleted { get; set; }
        [DynamoDBProperty]
        public short Language { get; set; }
    }
}
