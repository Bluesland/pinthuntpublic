﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models
{
    class EnumModel
    {
    }
    public enum ResultCodes
    {
        Success = 0,
        Error = 1,
        LoginErrorInvalidUser = 2,
        LoginErrorInvalidPasswordOrUser = 3
    }
    public enum CommentRequestTypes
    {
        Add = 0,
        Update = 1,
        Remove = 2
    }
}
