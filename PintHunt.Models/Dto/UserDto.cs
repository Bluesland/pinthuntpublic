﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.Dto
{
    public class UserDto : DbModels.User
    {
        public UserDto()
        {

        }
        public UserDto(DbModels.User user)
        {
            CreatedBy = user.CreatedBy;
            CreatedOn = user.CreatedOn;
            Id  = user.Id;
            IsActive = user.IsActive;
            IsDeleted = user.IsDeleted;
            Language = user.Language;
            ModifiedBy = user.ModifiedBy;
            ModifiedOn = user.ModifiedOn;
            Name = user.Name;
            PasswordHash = user.PasswordHash;
            ProfilePicture = user.ProfilePicture;
            Scores = user.Scores;
            UserName = user.UserName;
        }
        public string Password { get; set; }
        public DbModels.User GetDynamoModel()
        {
            return new DbModels.User()
            {
                CreatedBy = CreatedBy,
                CreatedOn = CreatedOn,
                Id = Id,
                IsActive = IsActive,
                IsDeleted = IsDeleted,
                Language = Language,
                ModifiedBy = ModifiedBy,
                ModifiedOn = ModifiedOn,
                Name = Name,
                PasswordHash = PasswordHash,
                ProfilePicture = ProfilePicture,
                Scores = Scores,
                UserName = UserName
            };
        }
    }
}
