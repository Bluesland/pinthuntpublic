﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.Dto
{
    public class RouteDto : DbModels.Route
    {
        public RouteDto()
        {

        }
        public RouteDto(DbModels.Route route)
        {
            CreatedBy = route.CreatedBy;
            CreatedOn = route.CreatedOn;
            Id  = route.Id;
            IsActive = route.IsActive;
            IsDeleted = route.IsDeleted;
            Language = route.Language;
            ModifiedBy = route.ModifiedBy;
            ModifiedOn = route.ModifiedOn;
            Name = route.Name;
            Pubs = route.Pubs;
            StartTime = route.StartTime;  
            EndTime = route.EndTime;    
            PintLimit = route.PintLimit ; 
            Latitude = route.Latitude ;  
            Longitude = route.Longitude ; 
            RateCount = route.RateCount ;
            Rate = route.Rate;


        }
        
        public DbModels.Route GetDynamoModel()
        {
            return new DbModels.Route()
            {
                CreatedBy = CreatedBy,
                CreatedOn = CreatedOn,
                Id = Id,
                IsActive = IsActive,
                IsDeleted = IsDeleted,
                Language = Language,
                ModifiedBy = ModifiedBy,
                ModifiedOn = ModifiedOn,
                Name = Name,
                Pubs       = Pubs,
                StartTime  = StartTime,             
                EndTime    = EndTime,            
                PintLimit  = PintLimit ,         
                Latitude   = Latitude ,        
                Longitude  = Longitude ,        
                RateCount  = RateCount ,      
                Rate       = Rate     

            };
        }
    }
}


