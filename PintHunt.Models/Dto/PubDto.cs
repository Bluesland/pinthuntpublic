﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Models.Dto
{
    public class PubDto : DbModels.Pub
    {
        public PubDto()
        {

        }
        public PubDto(DbModels.Pub pub)
        {
            CreatedBy = pub.CreatedBy;
            CreatedOn = pub.CreatedOn;
            Id  = pub.Id;
            IsActive = pub.IsActive;
            IsDeleted = pub.IsDeleted;
            Language = pub.Language;
            ModifiedBy = pub.ModifiedBy;
            ModifiedOn = pub.ModifiedOn;
            Name = pub.Name;
            Address = pub.Address;
            Latitude = pub.Latitude;
            Longitude = pub.Longitude;
            Description = pub.Description;
            RateCount = pub.RateCount;
            Rate = pub.Rate;       
            Image = pub.Image;


        }
        
        public DbModels.Pub GetDynamoModel()
        {
            return new DbModels.Pub()
            {
                CreatedBy = CreatedBy,
                CreatedOn = CreatedOn,
                Id = Id,
                IsActive = IsActive,
                IsDeleted = IsDeleted,
                Language = Language,
                ModifiedBy = ModifiedBy,
                ModifiedOn = ModifiedOn,
                Name = Name,
                Address     = Address,
                Latitude    = Latitude,    
                Longitude   = Longitude,   
                Description = Description, 
                RateCount   = RateCount,   
                Rate        = Rate,        
                Image       = Image       

            };
        }
    }
}



