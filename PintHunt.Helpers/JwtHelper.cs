﻿using JWT;
using JWT.Algorithms;
using JWT.Builder;
using PintHunt.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PintHunt.Helpers
{
    public class JwtHelper
    {
        public static string GenerateToken(TokenModel tokenModel)
        {
            var now = DateTime.UtcNow.AddMonths(1);
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var secondsSinceEpoch = Math.Round((now - unixEpoch).TotalSeconds);
            tokenModel.exp = secondsSinceEpoch;
            var secret = "080f7225-25d9-475f-904f-dcceb6a348e5";
            var token = new JwtBuilder()
                        .WithAlgorithm(new HMACSHA256Algorithm())
                        .WithSecret(secret)
                        .AddClaim("deviceType", tokenModel.deviceType)
                        .AddClaim("exp", tokenModel.exp)
                        .AddClaim("id", tokenModel.id)
                        .AddClaim("lang", tokenModel.lang)
                        .AddClaim("versionNo", tokenModel.versionNo).Build();
            return token;
        }
        public static TokenModel GetToken(string token)
        {
            var secret = "080f7225-25d9-475f-904f-dcceb6a348e5";

            try
            {
                var json = new JwtBuilder()
                    .WithSecret(secret)
                    .MustVerifySignature()
                    .Decode(token);
                return JsonConvert.DeserializeObject<TokenModel>(json);
            }
            catch (TokenExpiredException)
            {
                return null;
            }
            catch (SignatureVerificationException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
