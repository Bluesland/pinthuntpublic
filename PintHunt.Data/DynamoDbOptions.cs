﻿namespace PintHunt.Data
{
    public class DynamoDbOptions
    {   //test
        public string User { get; set; }
        public string Pub { get; set; }
        public string Route { get; set; }
        public string AdminUser { get; set; }
        public string RouteTrip { get; set; }
        public string Leaderboard { get; set; }
    }
}
